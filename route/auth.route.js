const router = require('express').Router()
const auth = require('../controller/auth.controller')
const { body, validationResult } = require('express-validator')

const authClass = new auth()


router.post('/signup', authClass.signup)

router.post('/login', authClass.login)

module.exports = router