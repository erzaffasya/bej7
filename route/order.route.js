const router = require('express').Router()
const order = require('../controller/order.controller')
const { body, validationResult } = require('express-validator')

const orderClass = new order()
router.get('/', (req, res, next) => {

    orderClass.getAllOrder()
        .then(data => {
            console.log
            return res.status(200).json({
                data
            })
        })
        .catch(err => {
            return res.status(400).json({
                data: err
            })

        })

})
router.get('/search', (req, res, next) => {
    // console.log(req.query)
    orderClass.search(req.query.id)
        .then(data => {
            return res.status(200).json({
                data
            })
        })
        .catch(err => {
            return res.status(400).json({
                data: err
            })

        })
})

router.get('/:id', (req, res, next) => {
    orderClass.getById(req.params.id)
        .then(data => {
            return res.status(200).json({
                data
            })
        })
        .catch(err => {
            return res.status(400).json({
                data: err
            })

        })
})

router.post('/', (req, res, next) => {
    orderClass.insertData(req.body)
        .then(data => {
            return res.status(200).json({
                data
            })
        })
        .catch(err => {
            return res.status(400).json({
                data: err
            })

        })
})

router.put('/:id', (req, res, next) => {
    orderClass.update(req.params.id, req.body)
        .then(data => {
            return res.status(200).json({
                data
            })
        })
        .catch(err => {
            return res.status(400).json({
                data: err
            })

        })
})

router.delete('/:id', (req, res, next) => {
    orderClass.deleteData(req.params.id)
        .then(data => {
            return res.status(200).json({
                data
            })
        })
        .catch(err => {
            return res.status(400).json({
                data: err
            })

        })
})

module.exports = router