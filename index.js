const express = require('express')
const itemsRoute = require('./route/item.route')
const ordersRoute = require('./route/order.route')
const authRoute = require('./route/auth.route')
var cookieParser = require('cookie-parser')
const port = process.env.PORT || 3000
const app = express()

require('dotenv').config()
app.use(cookieParser())
app.use(express.json())
app.use(express.urlencoded({
    extended: true
}))

app.get('/', function (req, res) {
    console.log('Cookies: ', req.cookies)
  
    console.log('Signed Cookies: ', req.signedCookies)
  })
  
app.use('/items', itemsRoute)

app.use('/auth', authRoute)

// app.use('/orders', ordersRoute)   ON PROGRESS

const db = require("./models");
// db.sequelize.sync()
//   .then(() => {
//     console.log("Synced db.");
//   })
//   .catch((err) => {
//     console.log("Failed to sync db: " + err.message);
//   });


app.listen(port, () => {
    if (process.env.ENV == 'DEV') {
        console.log(`halo ini ada di bagian untuk DEV`)
    } else if (process.env.ENV == 'PROD') {
        console.log(`halo ini ada di bagian untuk PROD`)
    } else {
        console.log(`ENV tidak terbaca`)
    }
    console.log(`BinarAcademy app listening on port ${port}`)
})