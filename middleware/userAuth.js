
const express = require("express");
const db = require("../models");
 const User = db.users;

 const saveUser = async (req, res, next) => {
 try {
   const name = await User.findOne({
     where: {
        name: req.body.name,
     },
   });
   if (name) {
     return res.json(409).send("name already taken");
   }

   const emailcheck = await User.findOne({
     where: {
       email: req.body.email,
     },
   });

   if (emailcheck) {
     return res.json(409).send("Authentication failed");
   }

   next();
 } catch (error) {
   console.log(error);
 }
};

//exporting module
 module.exports = {
 saveUser,
};